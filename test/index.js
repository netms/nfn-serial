"use strict";

let serial                                                = require("../index.js"),
	[name = "/dev/tty.usbserial-00001014A", speed = 9600] = process.argv.splice(2),
	com;

com = serial({
	log:   true,
	hex:   false,
	name:  name,
	speed: Number(speed)
});

com.on({
	"start": (name, cfg)=> console.log(`[open] name: ${name} cfg: ${cfg}\nplease send {reset|close|hello}\n`),
	"stop":  ()=> console.log("[close]"),
	"read":  (buf, len)=> {
		if (typeof(buf) == "string") {
			console.log(`[read] buf:${buf} len: ${len}`);

			switch (buf) {
				case "reset":
					com.reset();
					break;
				case "close":
					com.close();
					break;
				case "hello":
					console.log("[write] data:", com.write("this is serial test"));
					break;
			}
		}
		else {
			console.log("[read] buf:", buf, " len:", len);
		}
	},
	"write": len=> console.log("[write] len: ", len)
});

com.open();