"use strict";

module.exports = exports = ((serial, nfn)=> {
	let FN  = nfn.fn,
		NFN = nfn.NFN;

	let Serial = NFN.extend({
		init_custom:   function (opt, event, oper, EVENT, BAUD) {
			this.BAUD = BAUD;
		},
		option_custom: function (dest, opt, def) {
			let {
					speed, parity, stop, size, hw, sw
				}          = opt || {};

			def = def || {
					speed:  9600,
					parity: 0,
					stop:   1,
					size:   8,
					hw:     false,
					sw:     false
				};

			if (typeof(speed) != "number" || !FN.has(speed, this.BAUD)) speed = def.speed;
			switch ((parity + "").toUpperCase()) {
				default:
					parity = def.parity;
					break;
				case "NONE":
				case "N":
				case "0":
					parity = 0;
					break;
				case "ODD":
				case "O":
				case "1":
					parity = 1;
					break;
				case "EVEN":
				case "E":
				case "2":
					parity = 2;
					break;
			}
			if (stop !== 1 && stop !== 2) stop = def.stop;
			if (size !== 8 && size !== 5 && size !== 6 && size !== 7) size = def.size;

			hw = typeof(hw) == "boolean" ? hw === true : def.hw;
			sw = typeof(sw) == "boolean" ? sw === true : def.sw;

			Object.assign(dest, {speed, parity, stop, size, hw, sw});

			return dest;
		},
		config_custom: function (mode, opt, key, value) {
			let result;

			switch (mode) {
				case 0:
					result = {
						name:   opt.name,
						speed:  opt.speed,
						parity: opt.parity,
						stop:   opt.stop,
						size:   opt.size
					};

					if (result.parity == 1) result.parity = "ODD";
					else if (result.parity == 2) result.parity = "EVEN";
					else result.parity = "NONE";

					return result;
				case 2:
					this.reset();
					break;
			}
		}
	});

	let init   = function (opt, event) {
			return new Serial(opt, event,
				serial,
				["start", "stop", "read", "write"],
				[2400, 4800, 9600, 19200, 38400, 57600, 115200]);
		},
		result = function (opt, event) { return init(opt, event); };

	result.init = init;

	return result;
// })(require("./bin/serial.node"), require("./src/nfn.js"));
})(require("./src/build/Release/serial.node"), require("./src/nfn.js"));